import React, { useState }from 'react';

const Controls = ({ onChildClick }) => {

  const [observer, setObserver] = useState(false);

  var loginButton;
    if (observer) {
      loginButton = "observer.png";
    } else {
      loginButton = "observer_delete.png";
    }

    return (
      
      <div className="child sticky-top">
        <div className="card-horizontal">
          <center>
            <button className="btn btn-primary" data_but="btn-xs" type="button" data-toggle="tooltip" data-placement="bottom" title="Tooltip on bottom" onClick={() => {createAnimal("cow")}}><i className='fa fa-heart'><img className="img img-control" src={require('../assets/icons/cow2.png')} alt="Add cow"/></i></button>
            <button className="btn btn-primary" data_but="btn-xs" onClick={() => {createAnimal("duck")}}><i className='fa fa-heart'><img className="img img-control" src={require('../assets/icons/duck2.png')} alt="Add duck"/></i></button>
            <button className="btn btn-primary" data_but="btn-xs" onClick={() => {createAnimal("lion")}}><i className='fa fa-heart'><img className="img img-control" src={require('../assets/icons/lion2.png')} alt="Add lion"/></i></button>
            <button className="btn btn-primary" data_but="btn-xs" onClick={() => {createAnimal("monkey")}}><i className='fa fa-heart'><img className="img img-control" src={require('../assets/icons/monkey2.png')} alt="Add monkey"/></i></button>
          </center>
          <button className="btn-secondary" onClick={() => {handleObserver()}}><img className="img img-secondary" src={require("../assets/icons/"+loginButton)} alt="Add Observer"/></button>
          <button className="btn-secondary" onClick={() => {closeZoo()}}><img className="img img-secondary" src={require('../assets/icons/zoo_closed.png')} alt="Close Zoo"/></button>          
          <button className="btn-secondary" onClick={() => {clearAnimals()}}><img className="img img-secondary" src={require('../assets/icons/recycle.png')} alt="Clear Animals"/></button>          
        </div>
      </div>
    );

    function createAnimal(anim) {
      console.log("Create "+anim);
      fetch('http://localhost:8080/api/v1/animals/create/'+anim, {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
      }})
        .then((data) => {
          onChildClick();
        })
        .catch(console.log);
    }

    function clearAnimals() {
      console.log("Clear animals ");
      fetch('http://localhost:8080/api/v1/animals/clear', {
        method: 'DELETE',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
      }})
        .then((data) => {
          onChildClick();
        })
        .catch(console.log);
    }

    function closeZoo() {
      console.log("Close zoo ");
      fetch('http://localhost:8080/api/v1/animals/closezoo', {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
      }})
        .then((data) => {
          //onChildClick();
          alert("Zoo closed");
        })
        .catch(console.log);
    }

    function handleObserver() {
      console.log("Handle observer ");
      if(observer){
        clearObservers();
      } else {
        addObserver();
      }
    }

    function addObserver() {
      console.log("Add observer ");
      fetch('http://localhost:8080/api/v1/animals/addzooobserver/zooobserver', {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
      }})
        .then((data) => {
          setObserver(!observer);
          //onChildClick();
        })
        .catch(console.log);
    }

    function clearObservers() {
      console.log("Clear observers ");
      fetch('http://localhost:8080/api/v1/animals/removeallzooobserver', {
        method: 'DELETE',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
      }})
        .then((data) => {
          setObserver(false);
          //onChildClick();
        })
        .catch(console.log);
    }

  }

  export default Controls