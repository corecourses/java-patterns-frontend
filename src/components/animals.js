import React from 'react';

    const Animals = ({ animals, onChildClick}) => {
        
      return (
        <div>
          <center><h1> </h1></center>
          {animals.map((animal) => (         
            <div className="container-fluid" key={animal.id}>
                <div className="row">
                    <div className="col-12 mt-3">
                        <div className="card">
                            <div className="card-horizontal">
                                <div className="img-card">
                                {(() => {
                                    switch(animal.name) {
                                        case "Duck": return <img className="img-a" src={require('../assets/icons/duck.png')} alt="Card duck"/>;
                                        case "Monkey": return <img className="img-a" src={require('../assets/icons/monkey.png')} alt="Card monkey"/>;
                                        case "Cow": return <img className="img-a" src={require('../assets/icons/cow.png')} alt="Card cow"/>;
                                        case "Lion": return <img className="img-a" src={require('../assets/icons/lion.png')} alt="Card lion"/>;
                                        default: return ;
                                    }
                                })()}                                    
                                </div>
                                <div className="card-horizontal">
                                    <div className="card-body card-detail">
                                        <h4 className="card-title">{animal.name}</h4>
                                        <p className="card-text">{animal.id}</p>
                                        <p className="card-text">{animal.weight} kg</p>
                                        <p className="card-text">{animal.legs} legs</p>
                                        <p className="card-text">{animal.lastAction} &nbsp;</p>                                        
                                    </div>
                                    <div className="card-body card-button-detail">
                                        <p ><button className="btn btn-m btn-success button-actions" onClick={() => {doAction(animal, "jump")}}>Jump</button></p>
                                        <p ><button className="btn btn-m btn-danger button-actions" onClick={() => {doAction(animal, "walk")}}>Walk</button></p>
                                        <p ><button className="btn btn-m btn-warning button-actions" onClick={() => {doAction(animal, "eat")}}>Eat</button></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>            
          ))}
        </div>
      )

      function doAction(animal, action) {
        console.log("Action "+animal.id+" "+action);
        fetch('http://localhost:8080/api/v1/animals/doanimalaction/'+ animal.id +'/'+action, {
          method: 'POST',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        }})
          .then((data) => {
            onChildClick();
          })
          .catch(console.log);
      }

    };

    

    export default Animals