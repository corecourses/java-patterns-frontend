import React, {Component} from 'react';
import Animals from './components/animals';
import Controls from './components/controls';
require('./App.css');

    class App extends Component {

      constructor( props ){
        super( props );
        this.refreshAnimals = this.refreshAnimals.bind(this);
      }

      state = {
        animals: []
      }

      componentDidMount() {
        this.refreshAnimals();
      }

      refreshAnimals() {
        console.log("Refresh animals");
        fetch('http://localhost:8080/api/v1/animals/all')
          .then(res => res.json())
          .then((data) => {
            this.setState({ animals: data });
          })
          .catch(console.log);
      }

      render () {
        return (
          <div>
            <Controls onChildClick={this.refreshAnimals} />
            <Animals animals={this.state.animals} onChildClick={this.refreshAnimals}/>
          </div>
        );
      }
    }

    export default App;



